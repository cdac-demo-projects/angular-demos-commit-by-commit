import { Component, OnChanges, OnInit } from '@angular/core';
import { time } from 'console';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnChanges, OnInit{

  msg: string = 'initial';
  
  constructor(){
    console.log("Constructor Parent")
    setTimeout(() => {
      this.msg = 'final';
    }, 5000);
  }
  ngOnInit(): void {
   console.log("On Init Parent")
  }

  ngOnChanges(){
    console.log("On Changes Parent")
  }


  buttonClick(){
    setInterval(() =>{
      this.msg = Date().toString();
    },1000)
    
  }

}
