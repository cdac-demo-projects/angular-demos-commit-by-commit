import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';

@Component({
  selector: 'app-child1',
  templateUrl: './child1.component.html',
  styleUrls: ['./child1.component.css']
})
export class Child1Component implements OnInit,OnChanges {

  @Input() data: string;
  count: number = 0;
  constructor() {
    console.log("Constructor Child")
   }

  ngOnInit(): void {
    console.log("On Init Child");
  }
  
  ngOnChanges() {
    console.log("On Changes Child");
    this.count++;
  }

}
